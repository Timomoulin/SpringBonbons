package com.bonbons.demo.controllers.Admin;

import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.services.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private ProduitService produitService;

    @Autowired
    private CategorieService categorieService;
    @Autowired
    private InventaireService inventaireService;

    @GetMapping("/admin")
    public String index(Model model){
        List<Categorie> categories = categorieService.index();
        model.addAttribute("categories",categories);
        return "admin/dashboard";
    }

    @GetMapping("/admin/categories/{id}/produits")
    public String produitsByCategorie(@PathVariable long id,Model model ){
        Categorie categorie= categorieService.show(id);
        model.addAttribute("categorie",categorie);
        return "admin/produits";
    }

    @GetMapping("/export/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=inventaire_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Categorie> inventaireList = categorieService.index();

        ExcelExporterService excelExporter = new ExcelExporterService(inventaireList);

        excelExporter.export(response);
    }

    @GetMapping("import/excel")
    public String formImport()
    {
        return "admin/import";
    }
    @PostMapping("/import/excel")
    public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {

        ExcelImportService importService=new ExcelImportService(new XSSFWorkbook( file.getInputStream()));
        importService.importer();
    }



}
