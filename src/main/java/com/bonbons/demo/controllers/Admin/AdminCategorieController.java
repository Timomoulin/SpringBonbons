package com.bonbons.demo.controllers.Admin;

import com.bonbons.demo.models.dto.CategorieDto;
import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.services.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminCategorieController {
    @Autowired
    CategorieService categorieService;

    @GetMapping("/categories")
    public String index(Model model){
        model.addAttribute("categories",categorieService.index());
        return "admin/categories/index";
    }

    @GetMapping("/categories/create")
    public String create(Model model){
        CategorieDto categorieDto = new CategorieDto();
        model.addAttribute("categorieDto",categorieDto);
        return "admin/categories/create";
    }

    @PostMapping("/categories")
    public String store(@ModelAttribute CategorieDto categorieDto, Model model){
        categorieService.store(categorieDto);
        return "redirect:/admin/categories";
    }
}
