package com.bonbons.demo.controllers.Admin;

import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.models.entities.Produit;
import com.bonbons.demo.services.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AdminProduitController {
    @Autowired
private ProduitService produitService;
    @GetMapping("/produits")
    public String index(Model model){
        List<Produit> produits=produitService.all();
        model.addAttribute("lesProduits",produits);
        return "admin/produits/index";
    }
}
