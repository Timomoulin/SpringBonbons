package com.bonbons.demo.controllers;

import com.bonbons.demo.models.entities.Produit;
import com.bonbons.demo.services.ProduitService;
import com.bonbons.demo.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@org.springframework.stereotype.Controller
public class Controller {
    @Autowired
    private ProduitService produitService;

    @Autowired
    private UtilisateurService utilisateurService;

    @GetMapping("/")
    public String index(Model model){
        List<Produit> bonbons = produitService.all();
        model.addAttribute("produits",bonbons);
        return "pageProduits";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/register")
    public  String register(Model model){
    utilisateurService.register("admin@email.com","admin123");
    return "redirect:./";
    }


}
