package com.bonbons.demo.services;

import com.bonbons.demo.models.entities.Utilisateur;
import com.bonbons.demo.models.repositories.RoleRepository;
import com.bonbons.demo.models.repositories.UtilisateurRepository;
import org.slf4j.helpers.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UtilisateurService {



@Autowired
    UtilisateurRepository utilisateurRepository;
@Autowired
RoleRepository roleRepository;

    public void register(String email, String mdp){
        Utilisateur utilisateur=new Utilisateur();
        utilisateur.setEmail(email);
        utilisateur.setMdp("{bcrypt}"+new BCryptPasswordEncoder().encode(mdp));
        utilisateur.setRole(roleRepository.getById(1L));
        utilisateurRepository.save(utilisateur);
    }
}
