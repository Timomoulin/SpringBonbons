package com.bonbons.demo.services;

import com.bonbons.demo.models.entities.Inventaire;
import com.bonbons.demo.models.entities.Produit;
import com.bonbons.demo.models.repositories.InventaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventaireService {

@Autowired
    private InventaireRepository inventaireRepository;

public List<Inventaire> all(){
    return inventaireRepository.findAll();
}
}
