package com.bonbons.demo.services;

import com.bonbons.demo.models.dto.CategorieDto;
import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.models.repositories.CategorieRepository;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class CategorieService {

    @Autowired
    private CategorieRepository repository;

    /**
     * Recupere une liste de Categorie
     *
     * @return List<Categorie>
     */
    public List<Categorie> index() {
        return repository.findAll();
    }

    /**
     * Recupere un Categorie pas son id
     *
     * @param id
     * @return un objet Categorie
     */
    public Categorie show(long id) {
        return repository.findById(id).get();
    }

    /**
     * Enregistre un nouveau Categorie
     *
     * @param CategorieDto
     */
    public void store(CategorieDto dto) {
        Categorie newCategorie = this.convertDtotoModel(dto);
        repository.save(newCategorie);
    }

    /**
     * Met a jour un Categorie
     *
     * @param id
     * @param dto
     */
    public void update(Long id, CategorieDto dto) {
        if (null == id) {
            Categorie newCategorie = this.convertDtotoModel(dto);
            repository.save(newCategorie);
        }

    }

    /**
     * Supprime un Categorie
     *
     * @param id
     */
    public void destroy(Long id) {
        repository.deleteById(id);
    }

    /**
     * Convertion d'un DTO en Categorie
     *
     * @param dto
     * @return un objet Categorie
     */
    public Categorie convertDtotoModel(CategorieDto dto) {
        Categorie categorie= new Categorie();
        categorie.setId(dto.getId());
        categorie.setNom(Jsoup.parse(dto.getNom()).text());
        //categorie.setProduits(dto.getProduits());
        return  categorie;
    }

    /**
     * convertion d'un CategorieDto en Categorie
     *
     * @param Categorie un objet Categorie
     * @return un CategorieDto
     */
    public CategorieDto convertModeltoDto(Categorie model) {
        CategorieDto categorieDto= new CategorieDto();
        categorieDto.setId(model.getId());
        categorieDto.setNom(Jsoup.parse(model.getNom()).text());

        //categorieDto.setProduits(model.getProduits());
        return  categorieDto;
    }


}
