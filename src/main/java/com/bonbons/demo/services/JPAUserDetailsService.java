package com.bonbons.demo.services;


import com.bonbons.demo.models.entities.Utilisateur;
import com.bonbons.demo.models.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class JPAUserDetailsService implements UserDetailsService {
@Autowired
private UtilisateurRepository utilisateurRepository;

    @Bean
    public PasswordEncoder delegatingPasswordEncoder() {
        PasswordEncoder defaultEncoder = new BCryptPasswordEncoder();
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());

        DelegatingPasswordEncoder passworEncoder = new DelegatingPasswordEncoder(
                "bcrypt", encoders);
        passworEncoder.setDefaultPasswordEncoderForMatches(defaultEncoder);

        return passworEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //Recuperer l'utilisateur pas son email
        Utilisateur utilisateur = utilisateurRepository.findByEmail(username);
        //Si l'utilisateur est null
        if(null==utilisateur){
            //alors on jette une execption
            throw new UsernameNotFoundException("Utilisateur introuvable");
        }
        //Instancier une un set de d'autorités/permisions
        Set<GrantedAuthority> listeDePermisions= new HashSet<GrantedAuthority>();
//        String nomRole=utilisateur.getRole().getNom();
//        GrantedAuthority unePermision= new SimpleGrantedAuthority(nomRole);
//        listeDePermisions.add(unePermision);

         //On ajoute une permision a la liste de permision
        //La permesion que l'on ajoute correspond au nom du role de l'utilisateur
        listeDePermisions.add(new SimpleGrantedAuthority(utilisateur.getRole().getNom()));
        //On retourne un User (de user details) avec un login/email un mdp et listePermisions
BCryptPasswordEncoder encoder= new BCryptPasswordEncoder();
     //PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    String password= encoder.encode("admin123");
        System.out.println(password);
    System.out.println("PASSWORD = "+encoder.encode(utilisateur.getMdp()));
        return new User(utilisateur.getEmail(),utilisateur.getMdp(),listeDePermisions);
    }
}
