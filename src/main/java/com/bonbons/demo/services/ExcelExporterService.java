package com.bonbons.demo.services;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.models.entities.Inventaire;
import com.bonbons.demo.models.entities.Produit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExporterService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<Inventaire> listInventaire;
    private List<Categorie> listCategories;

    private CellStyle headerStyle;

//    public ExcelExporterService(List<Inventaire> listInventaire) {
//        this.listInventaire = listInventaire;
//        workbook = new XSSFWorkbook();
//        //Création d'un style pour les entetes
//        createHeaderStyle();
//    }

    public ExcelExporterService(List<Categorie> categories) {
        this.listCategories = categories;
        workbook = new XSSFWorkbook();
        //Création d'un style pour les entetes
        createHeaderStyle();
    }

    private void createHeaderStyle() {
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
        headerStyle = style;
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Categories");

        Row row = sheet.createRow(0);

        createCell(row, 0, "Categorie", headerStyle);
        createCell(row, 1, "Produit", headerStyle);
        createCell(row, 2, "Portion", headerStyle);
        createCell(row, 3, "Prix", headerStyle);
        createCell(row, 4, "Quantite", headerStyle);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Float) {
            cell.setCellValue((Float) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (Categorie categorie : listCategories) {
            if(categorie.getProduits().size()==0){
                continue;
            }
            int columnCount = 0;
            int catMin = rowCount;

            for (Produit produit : categorie.getProduits()
            ) {
                if(produit.getInventaires().size()==0){
                    continue;
                }
                int produitMin = rowCount;

                for (Inventaire inventaire : produit.getInventaires()
                ) {
                    columnCount=0;
                    Row row = sheet.createRow(rowCount++);
                    createCell(row, columnCount++, inventaire.getProduit().getCategorie().getNom(), style);
                    createCell(row, columnCount++, inventaire.getProduit().getNom(), style);
                    createCell(row, columnCount++, inventaire.getPortion(), style);
                    createCell(row, columnCount++, inventaire.getPrix(), style);
                    createCell(row, columnCount++, inventaire.getQuantite(), style);
                }
                int produitMax = rowCount;
                System.out.println("Produit = "+produitMin+" || "+produitMax);
                sheet.addMergedRegion(new CellRangeAddress(produitMin,produitMax-1,1,1));
            }

            int catMax = rowCount;
            System.out.println("Categorie = "+catMin+" || "+catMax);
            sheet.addMergedRegion(new CellRangeAddress(catMin,catMax-1,0,0));
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
