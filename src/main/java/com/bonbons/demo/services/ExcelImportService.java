package com.bonbons.demo.services;

import com.bonbons.demo.models.dto.ExcelCommandeDto;
import com.bonbons.demo.models.dto.ExcelLigneCommandeDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.*;


public class ExcelImportService {
    private XSSFWorkbook workbook;
    List<ExcelCommandeDto> excelCommandeDtoList;

    public ExcelImportService(XSSFWorkbook workbook) {
        this.workbook = workbook;
        excelCommandeDtoList = new ArrayList<ExcelCommandeDto>();
    }


    private void generateDto() {
        Sheet sheet = workbook.getSheetAt(0);

        HashMap<Integer, String> hearder = new HashMap<Integer, String>();
        Row headerRow = sheet.getRow(sheet.getTopRow());
        Iterator<Cell> cellIterator = headerRow.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            hearder.put(cell.getColumnIndex(), cell.getStringCellValue().toLowerCase());
        }

        ExcelCommandeDto commandeDto = null;

        Date dateCommande = null;
        String statut = null;
        String client = null;
        //Parcourir les lignes
        for (int nbRow = 1; nbRow <= sheet.getLastRowNum(); nbRow++) {

            int quantite = 0;
            long idInventaire = 0;
            Iterator<Cell> cellIterator2 = sheet.getRow(nbRow).cellIterator();
            //Parcourir les cellules
            while (cellIterator2.hasNext()) {
                Cell cell = cellIterator2.next();
                switch (hearder.get(cell.getColumnIndex())) {
                    case "date commande":
                        //En cas de merge les cellules sont considerer avec des valeurs par défauts
                        if (null != cell.getDateCellValue()) {
                            dateCommande = cell.getDateCellValue();
                        }
                        break;
                    case "statut":
                        if ("" != cell.getStringCellValue()) {
                            statut = cell.getStringCellValue();
                        }
                        break;
                    case "client":
                        if ("" != cell.getStringCellValue()) {
                            client = cell.getStringCellValue();
                        }
                        break;
                    case "quantite":
                        quantite = (int) cell.getNumericCellValue();
                        break;
                    case "idinventaire":
                        idInventaire = (long) cell.getNumericCellValue();
                        break;
                }
            }
            ExcelCommandeDto newExcelCommandeDto = new ExcelCommandeDto(dateCommande, statut, client);
            if (null == commandeDto || !newExcelCommandeDto.equals(commandeDto)) {
                commandeDto = newExcelCommandeDto;
                excelCommandeDtoList.add(commandeDto);
            }
            commandeDto.getExcelLigneCommandeDtoSet().add(new ExcelLigneCommandeDto(quantite, idInventaire));
            System.out.println("Ligne " + nbRow);
        }
    }

    public void importer() {
        this.generateDto();
        System.out.println("Test");
    }
}
