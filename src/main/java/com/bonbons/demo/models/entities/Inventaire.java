/*
* This code has been generated by the Rebel: a code generator for modern Java.
*
* Drop us a line or two at feedback@archetypesoftware.com: we would love to hear from you!
*/
package com.bonbons.demo.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;
import java.time.*;





@Getter
@Setter
@NoArgsConstructor
@Entity
public class Inventaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    private int quantite;


    private float prix;


    private String portion;

@ManyToOne
    private Produit produit;

    @OneToMany(mappedBy = "commande")
    private List<LigneCommande> ligneCommandes;




}