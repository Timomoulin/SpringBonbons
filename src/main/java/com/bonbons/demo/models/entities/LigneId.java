package com.bonbons.demo.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class LigneId implements Serializable {

    @Column(name = "commande_id")
    private long commandeId;
    @Column(name = "inventaire_id")
    private long inventaireId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LigneId ligneId = (LigneId) o;

        if (commandeId != ligneId.commandeId) return false;
        return inventaireId == ligneId.inventaireId;
    }

    @Override
    public int hashCode() {
        int result = (int) (commandeId ^ (commandeId >>> 32));
        result = 31 * result + (int) (inventaireId ^ (inventaireId >>> 32));
        return result;
    }
}
