package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.models.entities.Produit;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit, Long> {


    @Query(value = "select * from produit order by categorie_id,nom",nativeQuery = true)
    public List<Produit> foobar();

    List<Produit> findByOrderByCategorie_NomAscNomAsc();


}