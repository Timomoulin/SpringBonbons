package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.Categorie;
import com.bonbons.demo.models.entities.Inventaire;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventaireRepository extends JpaRepository<Inventaire, Long> {

    List<Inventaire>  findInventaireByProduit_Categorie(Categorie categorie);
}