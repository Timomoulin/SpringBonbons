package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandeRepository extends JpaRepository<Commande, Long> {
}