package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}