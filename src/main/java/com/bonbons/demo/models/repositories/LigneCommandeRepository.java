package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.LigneCommande;
import com.bonbons.demo.models.entities.LigneId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LigneCommandeRepository extends JpaRepository<LigneCommande, LigneId> {
}