package com.bonbons.demo.models.repositories;

import com.bonbons.demo.models.entities.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {
}