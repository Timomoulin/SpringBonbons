package com.bonbons.demo.models.dto;

import com.bonbons.demo.models.entities.Produit;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CategorieDto implements Serializable {
    private long id;
    private String nom;
    //private List<Produit> produits = new ArrayList<Produit>();
}
